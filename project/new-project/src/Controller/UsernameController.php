<?php

namespace App\Controller;

use App\Entity\UserName;
use App\Form\UserNameType;
use App\Repository\UserNameRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/username')]
class UsernameController extends AbstractController
{
    #[Route('/', name: 'app_username_index', methods: ['GET'])]
    public function index(UserNameRepository $userNameRepository): Response
    {
        return $this->render('username/index.html.twig', [
            'user_names' => $userNameRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_username_new', methods: ['GET', 'POST'])]
    public function new(Request $request, UserNameRepository $userNameRepository): Response
    {
        $userName = new UserName();
        $form = $this->createForm(UserNameType::class, $userName);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userNameRepository->add($userName);
            return $this->redirectToRoute('app_username_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('username/new.html.twig', [
            'user_name' => $userName,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_username_show', methods: ['GET'])]
    public function show(UserName $userName): Response
    {
        return $this->render('username/show.html.twig', [
            'user_name' => $userName,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_username_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, UserName $userName, UserNameRepository $userNameRepository): Response
    {
        $form = $this->createForm(UserNameType::class, $userName);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userNameRepository->add($userName);
            return $this->redirectToRoute('app_username_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('username/edit.html.twig', [
            'user_name' => $userName,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_username_delete', methods: ['POST'])]
    public function delete(Request $request, UserName $userName, UserNameRepository $userNameRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$userName->getId(), $request->request->get('_token'))) {
            $userNameRepository->remove($userName);
        }

        return $this->redirectToRoute('app_username_index', [], Response::HTTP_SEE_OTHER);
    }
}
